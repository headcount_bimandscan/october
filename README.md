# October (Point Cloud/Mesh Interaction Library) #

Fork of DURAARK library for point cloud compression & mesh interaction.

Includes fixes for OpenCV 3 and tweaked versions of PrimitiveShapes.
